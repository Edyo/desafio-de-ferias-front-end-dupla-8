import Image from "next/image";
import { FooterSection } from "../styles/styles";

export default function Footer() {
    return (
        <FooterSection>
            <div id="footer-cima">
                <div id="logo-footer"><Image src="/Logo.svg" alt="" width={220} height={90} /></div>
                <div id="footer-links-container">
                    <a href="#"><div><Image src="/Icon_Linkedin.svg" alt="" width={45} height={45} /></div></a>
                    <a href="#"><div><Image src="/Icon_Behance.svg" alt="" width={45} height={45} /></div></a>
                    <a href="#"><div><Image src="/Icon_Dribble.svg" alt="" width={45} height={45} /></div></a>
                    <a href="#"><div><Image src="/Icon_Instagram.svg" alt="" width={45} height={45} /></div></a>
                    <a href="#"><div><Image src="/Icon_Facebook.svg" alt="" width={45} height={45} /></div></a>
                    <a href="#"><div><Image src="/Icon_Youtube.svg" alt="" width={45} height={45} /></div></a>
                    <a href="#"><div><Image src="/Icon_Spotify.svg" alt="" width={45} height={45} /></div></a>
                </div>
            </div>
            <p id="text-contatos"><span>Email:</span> contato@infojr.com.br | <span>Telefone:</span>  71 3283-6268</p>
            <p id="text-copyright">Copyright &copy; 2020 INFO JR UFBA - Av. Adhemar de Barros, Ondina – Instituto de computação da UFBA</p>
        </FooterSection>
    );
}