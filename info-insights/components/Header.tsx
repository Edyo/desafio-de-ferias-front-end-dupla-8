import { BtnHeader, Hamburguer, LinkContainer, LinkNav, Modal, Navbar } from "../styles/styles"
import { useState } from 'react'
import Image from "next/image";
import Link from "next/link";

interface HeaderProps {
    page: string;
}

export default function Header(props: HeaderProps) {
    const [Active, setActive] = useState(false);

    function hamburguer() {
        setActive(!Active);
    }

    return (
        <Navbar>
            <Link href="/">
                <a id="logo"><Image src="/Logo.svg" alt="" layout="fill"/></a>
            </Link>
            <LinkContainer active={Active}>
                <Link href="/" passHref>
                    <LinkNav page={props.page == 'inicio' ? true : false} >Início</LinkNav>
                </Link>
                <Link href="/todos" passHref>
                    <LinkNav page={props.page == 'todos' ? true : false} >Todos os links</LinkNav>
                </Link>
                <Link href="/adicionar" passHref>
                    <BtnHeader>Adicionar link</BtnHeader>
                </Link>
            </LinkContainer>
            <Hamburguer src="/menu.svg" onClick={hamburguer} active={Active} >
                <Image src={Active ? "/x.svg" : "/menu.svg"} alt="" layout="fill"/>
            </Hamburguer>
            <Modal active={Active} onClick={hamburguer} />
        </Navbar>
    )
}