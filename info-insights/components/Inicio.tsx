import { Btn, Card, InicioDir, InicioEsq, InicioSection, Paragraph } from "../styles/styles";
import { useState, useRef, useEffect } from "react";
import Image from "next/image";
import Link from "next/link";

interface InicioProps {
    insights: JSON;
}

export default function Inicio(props: InicioProps) {
    const [isDown, setisDown] = useState(false);
    const [yInicial, setyInicial] = useState(null);
    const [ScrollCima, setScrollCima] = useState(null)
    const dragScroll = useRef(null)

    //drag and scroll
    function mouseDown(e) {
        setisDown(true)
        setyInicial(e.pageY - dragScroll.current.offsetTop)
        setScrollCima(dragScroll.current.scrollTop)
    }

    function mouseLeave() {
        setisDown(false)
    }

    function mouseUp() {
        setisDown(false)
    }

    function mouseMove(e) {
        if (!isDown) {
            return;
        }
        e.preventDefault();
        let y = e.pageY - dragScroll.current.offsetTop;
        let scroll = y - yInicial;
        dragScroll.current.scrollTop = ScrollCima - scroll;
    }
    //fim do drag and scroll

    return (
        <InicioSection>
            <InicioEsq>
                <Image id="inicio-logo" src="/insightsLogo.svg" alt="" width={257} height={59} layout="fixed" />
                <Paragraph>A internet possibilita o infinito acesso a todo tipo de conhecimento.<br/><br/>Por isso, iremos catalogar 
                ideias, sites, cursos, aulas, livros, assuntos, palestras, toda sorte de links que sejam úteis e bons para conhecimento.
                <br/><br/>Comece a contribuir clicando no botão “<span>adicionar link</span>”.</Paragraph>
                <Link href="/adicionar" passHref >
                    <Btn id="btn-add" color="#7CB124" >Adicionar link</Btn>
                </Link>
                <Link href="/todos" passHref >
                    <Btn id="btn-todos" color ="#1A1C17" >Ver todos os links</Btn>
                </Link>
                <p id="text-info">Feito com 💚 <span>Info Jr UFBA</span></p>
            </InicioEsq>
            <InicioDir ref={dragScroll} onMouseDown={mouseDown} onMouseLeave={mouseLeave} onMouseUp={mouseUp} onMouseMove={mouseMove} >
                <Card>
                    <div className="card-img"><Image src="/img1.svg" alt="" width={80} height={80} className="image" /></div>
                    <div>
                        <p className="titulo-card">{props.insights[0].titulo}</p>
                        <p className="categoria">{props.insights[0].categoria}</p>
                        <Paragraph>{props.insights[0].mensagem}</Paragraph>
                        <Link href={"/insights/" + props.insights[0].id} passHref><a>Ir para Ideia</a></Link>
                    </div>
                </Card>
                <Card>
                <div className="card-img"><Image src="/img1.svg" alt="" width={80} height={80} className="image" /></div>
                    <div>
                        <p className="titulo-card">{props.insights[1].titulo}</p>
                        <p className="categoria">{props.insights[1].categoria}</p>
                        <Paragraph>{props.insights[1].mensagem}</Paragraph>
                        <Link href={"/insights/" + props.insights[1].id} passHref><a>Ir para Ideia</a></Link>
                    </div>
                </Card>
                <Card>
                <div className="card-img"><Image src="/img1.svg" alt="" width={80} height={80} className="image" /></div>
                    <div>
                        <p className="titulo-card">{props.insights[2].titulo}</p>
                        <p className="categoria">{props.insights[2].categoria}</p>
                        <Paragraph>{props.insights[2].mensagem}</Paragraph>
                        <Link href={"/insights/" + props.insights[2].id} passHref><a>Ir para Ideia</a></Link>
                    </div>
                </Card>
                <Card>
                <div className="card-img"><Image src="/img1.svg" alt="" width={80} height={80} className="image" /></div>
                    <div>
                        <p className="titulo-card">{props.insights[3].titulo}</p>
                        <p className="categoria">{props.insights[3].categoria}</p>
                        <Paragraph>{props.insights[3].mensagem}</Paragraph>
                        <Link href={"/insights/" + props.insights[3].id} passHref><a>Ir para Ideia</a></Link>
                    </div>
                </Card>
                <Card>
                    <div className="card-img"><Image src="/img1.svg" alt="" width={80} height={80} className="image" /></div>
                    <div>
                        <p className="titulo-card">{props.insights[4].titulo}</p>
                        <p className="categoria">{props.insights[4].categoria}</p>
                        <Paragraph>{props.insights[4].mensagem}</Paragraph>
                        <Link href={"/insights/" + props.insights[4].id} passHref><a>Ir para Ideia</a></Link>
                    </div>
                </Card>
            </InicioDir>
        </InicioSection>
    );
}