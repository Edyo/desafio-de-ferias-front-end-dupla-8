import Image from "next/image";
import { AdicionarSection, SeletorCategoria } from "../styles/styles";
import { useState, useRef } from "react";
import Link from "next/link";


export default function MainAdicionar() {
    const [Active, setActive] = useState(false)
    const [Selected, setSelected] = useState("--")
    const [Posting, setPosting] = useState(false)

    const falhaTitulo = useRef(null);
    const falhaCategoria = useRef(null);
    const falhaLink = useRef(null);
    const falhaDescricao = useRef(null);
    const labelTitulo = useRef(null);
    const labelCategoria = useRef(null);
    const labelLink = useRef(null);
    const labelDescricao = useRef(null);
    const inputTitulo = useRef(null);
    const inputCategoria = useRef(null);
    const inputLink = useRef(null);
    const inputAlt = useRef(null);
    const inputDescricao = useRef(null);

    function validationInputs(e) {
        if (inputTitulo.current.value != '') {
            falhaTitulo.current.classList.remove('active')
            labelTitulo.current.classList.add('passou')
            labelTitulo.current.classList.remove('falhou')
            inputTitulo.current.classList.add('passou')
            inputTitulo.current.classList.remove('falhou')
        }
        if (Selected != '--') {
            falhaCategoria.current.classList.remove('active')
            labelCategoria.current.classList.add('passou')
            labelCategoria.current.classList.remove('falhou')
            inputCategoria.current.classList.add('passou')
            inputCategoria.current.classList.remove('falhou')
        }
        if (inputLink.current.value != '') {
            falhaLink.current.classList.remove('active')
            labelLink.current.classList.add('passou')
            labelLink.current.classList.remove('falhou')
            inputLink.current.classList.add('passou')
            inputLink.current.classList.remove('falhou')
        }
        if (inputDescricao.current.value != '') {
            falhaDescricao.current.classList.remove('active')
            labelDescricao.current.classList.add('passou')
            labelDescricao.current.classList.remove('falhou')
            inputDescricao.current.classList.add('passou')
            inputDescricao.current.classList.remove('falhou')
        }


        if (inputTitulo.current.value == '') {
            falhaTitulo.current.classList.add('active')
            labelTitulo.current.classList.add('falhou')
            labelTitulo.current.classList.remove('passou')
            inputTitulo.current.classList.remove('passou')
            inputTitulo.current.classList.add('falhou')
        }
        if (Selected == '--') {
            falhaCategoria.current.classList.add('active')
            labelCategoria.current.classList.add('falhou')
            labelCategoria.current.classList.remove('passou')
            inputCategoria.current.classList.remove('passou')
            inputCategoria.current.classList.add('falhou')
        }
        if (inputLink.current.value == '') {
            falhaLink.current.classList.add('active')
            labelLink.current.classList.add('falhou')
            labelLink.current.classList.remove('passou')
            inputLink.current.classList.remove('passou')
            inputLink.current.classList.add('falhou')
        }
        if (inputDescricao.current.value == '') {
            falhaDescricao.current.classList.add('active')
            labelDescricao.current.classList.add('falhou')
            labelDescricao.current.classList.remove('passou')
            inputDescricao.current.classList.remove('passou')
            inputDescricao.current.classList.add('falhou')
        }
    }

    function abrirSeletor(e) {
        setActive(!Active)
    }

    function Selecionar(e) {
        setSelected(e.target.value)
        setActive(false)
    }

    async function postForm(e) {
        const resGet = await fetch('/back/insights/todos')
        const dataGet = await resGet.json();
        let data = {
            id: dataGet,
            titulo: inputTitulo.current.value,
            categoria: Selected,
            url: inputLink.current.value,
            urlalt: inputAlt.current.value,
            mensagem: inputDescricao.current.value
            }
        if (inputTitulo.current.value != '' && Selected != '--' && inputLink.current.value != '' && inputDescricao.current.value != '') {
            setPosting(false);
            setPosting(true);
            await fetch('/back/insights/novoinsight/', {
                method: "POST",
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(data)  
            })
            .then(res => {
                if (res.status >= 400) {
                    throw new Error("Bad response from server")
                }
            })
            .catch(err => {
                console.error(err)
            })
        }
    }

    function fechaLoading(e) {
        setPosting(false);
    }

    return (
        <AdicionarSection>
            <div id="loading-container" className={Posting ? "active" : ""}>
                <div id="loading-text-div">
                    <Image onClick={fechaLoading} id="loading-x" src="/Close.svg" alt="" width={14} height={14}></Image>
                    <p id="loading-text">Link cadastrado com sucesso</p>
                </div>
                <div id="barra-loading">
                    <div id="barra-progresso"></div>
                </div>
            </div>
            <h1>Adicionar link</h1>
            <div id="form">
                <div id="div-form-cima">
                    <div id="div-form-titulo">
                        <label htmlFor="form-titulo" ref={labelTitulo}>Título do link</label>
                        <input type="text" id="form-titulo" placeholder="Escreva um título para o link" required ref={inputTitulo}/>
                        <p className="falha" ref={falhaTitulo}>Escreva alguma coisa</p>
                    </div>
                    <div id="div-form-categoria">
                        <label htmlFor="form-categoria" ref={labelCategoria}>Categoria</label>
                        <div id ="form-categoria" className={Active ? 'active' : ''} onClick={abrirSeletor} ref={inputCategoria}><span>{Selected}</span>
                        <div className="seta-select"></div>
                        </div>
                        <p className="falha" ref={falhaCategoria}>Selecione uma opção</p>
                        <SeletorCategoria id="seletor-filtros" active={Active}>
                            <label htmlFor="option--">--</label>
                            <input type="radio" id="option--" className="option" value="--" onClick={Selecionar}/>
                            <label htmlFor="option-site">Site</label>
                            <input type="radio" id="option-site" className="option" value="Site" onClick={Selecionar}/>
                            <label htmlFor="option-palestra">Palestra</label>
                            <input type="radio" id="option-palestra" className="option" value="Palestra" onClick={Selecionar}/>
                            <label htmlFor="option-curso">Curso</label>
                            <input type="radio" id="option-curso" className="option" value="Curso" onClick={Selecionar}/>
                            <label htmlFor="option-livro">Livro</label>
                            <input type="radio" id="option-livro" className="option" value="Livro" onClick={Selecionar}/>
                        </SeletorCategoria>
                    </div>
                </div>
                <div id="div-form-meio">
                    <div id="div-form-link">
                        <label htmlFor="form-link" ref={labelLink}>Link principal</label>
                        <input type="text" id="form-link" placeholder="Ex. https://www.linkutil.com" required ref={inputLink}/>
                        <p className="falha" ref={falhaLink}>Escreva alguma coisa</p>
                    </div>
                    <div id="div-form-alt">
                        <label htmlFor="form-alt">Link secundário (opcional)</label>
                        <input type="text" id="form-alt" placeholder="Escreva um link alternativo ao principal" ref={inputAlt}/>
                    </div>
                </div>
                <label htmlFor="form-description" ref={labelDescricao}>Digite uma descrição para este link</label>
                <textarea id="form-description" placeholder="Escreva uma descrição" required ref={inputDescricao}/>
                <p className="falha" id="falha-descricao" ref={falhaDescricao}>Escreva alguma coisa</p>
                <div id="div-form-btn">
                    <Link href="/" passHref>
                        <a id="form-cancel">Cancelar</a>
                    </Link>
                    <input type="submit" id="form-submit" value="Cadastrar" onClick={e => { validationInputs(e); postForm(e)}}/>
                </div>
            </div>
        </AdicionarSection>
    );
}