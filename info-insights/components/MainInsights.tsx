import Image from "next/image";
import { InsightsSection } from "../styles/styles";

interface InsightsProps {
    insight: JSON;
}

export default function MainInsights(props: InsightsProps) {
    return (
        <InsightsSection>
            <div id="insights-img"><Image id="insights-image" src="/insightsDefault.svg" alt="" layout="fill" /></div>
            <h1>{props.insight ? props.insight[0].titulo : ""}</h1>
            <p>{props.insight ? props.insight[0].mensagem : ""}</p>
            <a href={props.insight ? props.insight[0].url : ""}>Saiba mais</a>
        </InsightsSection>
    );
}