import Image from "next/image";
import { Card, Paragraph, SeletorCategoria, TodosSection } from "../styles/styles";
import { useState, useEffect, useRef } from "react";
import Link from "next/link";

interface TodosProps {
    insights: JSON
    categoria: string
}

export default function MainTodos(props: TodosProps) {
    const [Page, setPage] = useState(1);
    const [Active, setActive] = useState(false)
    const [Selected, setSelected] = useState(props.categoria)
    const [Width, setWidth] = useState(undefined)
    const listaTodos = useRef(null)
    var nInsights = 0;
    if (props.insights != undefined) {
        nInsights = Object.keys(props.insights).length
    }

    useEffect(() => {
        function resize() {
            setWidth(window.innerWidth)
        }

        window.addEventListener('resize', resize)
        resize()

        return () => {
            window.removeEventListener('resize', resize)
        }
    }, [])

    function allCards(page) {
        let listacards = []
        if (props.insights != undefined) {
            if (Width >= 769) {
                for (var i=(page-1)*10; i<nInsights && i<page*10; i++) {
                    listacards.push(
                        <Card key={"card" + i} className="card">
                            <div className="card-img"><Image src="/img1.svg" alt="/img1.svg" width={80} height={80} className="image" /></div>
                            <div>
                                <p className="titulo-card">{props.insights[i].titulo}</p>
                                <p className="categoria">{props.insights[i].categoria}</p>
                                <Paragraph>{props.insights[i].mensagem}</Paragraph>
                                <Link href={"/insights/" + props.insights[i].id} passHref><a>Ir para Ideia</a></Link>
                            </div>
                        </Card>
                    )
                }
            } else {
                for (var i=0; i<nInsights; i++) {
                    listacards.push(
                        <Card key={"card" + i} className="card">
                            <div className="card-img"><Image src="/img1.svg" alt="/img1.svg" width={80} height={80} className="image" /></div>
                            <div>
                                <p className="titulo-card">{props.insights[i].titulo}</p>
                                <p className="categoria">{props.insights[i].categoria}</p>
                                <Paragraph>{props.insights[i].mensagem}</Paragraph>
                                <Link href={"/insights/" + props.insights[i].id} passHref><a>Ir para Ideia</a></Link>
                            </div>
                        </Card>
                    )
                }
            }
        }
        return (listacards)
    }

    

    function abrirSeletor(e) {
        setActive(!Active)
    }

    function Selecionar(e) {
        setSelected(e.target.value)
        setActive(false)
    }

    function passaPagina(e) {
        if (Page < Math.ceil(nInsights/10)) {
            setPage(Page + 1)
        }
    }

    function voltaPagina(e) {
        if (Page > 1) {
            setPage(Page - 1)
        }
    }
    return (
        <TodosSection>
            <div id="todos-div">
                <h1>Todos os links</h1>
                <div id="filtro-container">
                    <label htmlFor="seletor-filtros">Filtro</label>
                    <button className={Active ? 'active' : ''} onClick={abrirSeletor}><span>{Selected}</span>
                    <div className="seta-select"></div>
                    </button>
                    <SeletorCategoria id="seletor-filtros" active={Active}>
                        <label htmlFor="option-todos">Todos</label>
                        <Link href="/todos" passHref><input type="radio" id="option-todos" className="option" value="Todos" onClick={Selecionar}/></Link>
                        <label htmlFor="option-site">Site</label>
                        <Link href="/todos/Site" passHref><input type="radio" id="option-site" className="option" value="Site" onClick={Selecionar}/></Link>
                        <label htmlFor="option-palestra">Palestra</label>
                        <Link href="/todos/Palestra" passHref><input type="radio" id="option-palestra" className="option" value="Palestra" onClick={Selecionar}/></Link>
                        <label htmlFor="option-curso">Curso</label>
                        <Link href="/todos/Curso" passHref><input type="radio" id="option-curso" className="option" value="Curso" onClick={Selecionar}/></Link>
                        <label htmlFor="option-livro">Livro</label>
                        <Link href="/todos/Livro" passHref><input type="radio" id="option-livro" className="option" value="Livro" onClick={Selecionar}/></Link>
                    </SeletorCategoria>
                </div>
                <div id="lista-todos" ref={listaTodos}> 
                {allCards(Page)}
                </div>
                <div id="seletor-pagina">
                    <div onClick={voltaPagina} id="seta-esq">
                        <Image src="/setinha.svg" alt ="" width={6} height={11} />
                    </div>
                    <p id="numero-pagina" className={"page" + Page.toString()}>{Page}</p>
                    <p>of</p>
                    <p id="numero-de-paginas">{Math.ceil(nInsights/10)}</p>
                    <div id="seta-dir" onClick={passaPagina}>
                        <Image src="/setinha.svg" alt ="" width={6} height={11} />
                    </div>
                </div>
            </div>
        </TodosSection>
    );
}