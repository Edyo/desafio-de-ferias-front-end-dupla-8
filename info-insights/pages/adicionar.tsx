import Head from "next/head";
import Footer from "../components/Footer";
import Header from "../components/Header";
import MainAdicionar from "../components/MainAdicionar";
import { GlobalStyle } from "../styles/styles";


const Adicionar = () => (
    <div>
        <Head>
            <title>Info Insights</title>
        </Head>
        <GlobalStyle />
        <Header page="form" />
        <MainAdicionar />
        <Footer />
    </div>
  )

export default Adicionar;