import Header from "../components/Header"
import { GlobalStyle } from "../styles/styles"
import Inicio from "../components/Inicio";
import Footer from "../components/Footer";
import Head from "next/head";

export const getStaticProps = async () => {
  const res = await fetch('https://esther-api-infosights.herokuapp.com/insights/todos')
  const data = await res.json();

  return {
      props: { insights: data },
      revalidate: false
  }
}


const IndexPage = ({ insights }) => (
  <div>
    <Head>
      <title>Info Insights</title>
    </Head>
    <GlobalStyle />
    <Header page="inicio" />
    <Inicio insights={insights}/>
    <Footer />
  </div>
)

export default IndexPage;
