import Head from "next/head";
import Footer from "../../components/Footer";
import Header from "../../components/Header";
import MainInsights from "../../components/MainInsights";
import { GlobalStyle } from "../../styles/styles";

export const getStaticPaths = async () => {
    const res = await fetch('https://esther-api-infosights.herokuapp.com/insights/todos')
    const data = await res.json();

    const paths = data.map(insight => {
        return {
            params: {id: insight.id.toString()}
        }
    })

    return {
        paths,
        fallback: true
    }
}

export const getStaticProps = async (context) => {
    const id = context.params.id
    const res = await fetch('https://esther-api-infosights.herokuapp.com/insight/id/' + id)
    const data = await res.json();

    if (data == "Ops, não há insights com esse id.") {
        return {
            notFound: true,
            revalidate: false
        }
    }

    return {
        props: {
            insight: data
        },
        revalidate: false
    }
}

const Insights = ({ insight }) => (
    <div>
        <Head>
            <title>Info Insights</title>
        </Head>
        <GlobalStyle />
        <Header page="form" />
        <MainInsights insight={insight}/>
        <Footer />
    </div>
  )

export default Insights;