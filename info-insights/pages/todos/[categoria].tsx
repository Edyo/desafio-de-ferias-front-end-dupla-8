import Head from "next/head";
import Footer from "../../components/Footer";
import Header from "../../components/Header";
import MainTodos from "../../components/MainTodos";
import { GlobalStyle } from "../../styles/styles";

export const getServerSideProps = async ({ params }) => {
    const categoria = params.categoria
    const res = await fetch('https://esther-api-infosights.herokuapp.com/insights/categoria/' + categoria)
    const data = await res.json();

    if (data == "Não há insgths dentro dessa categoria") {
        return {
            props: {
                categoria: categoria
            },
        }
    }
    
    return {
        props: {
            insights: data,
            categoria: categoria
        },
    }
}

const Categoria = ({ insights, categoria }) => (
    <div>
        <Head>
            <title>Info Insights</title>
        </Head>
        <GlobalStyle />
        <Header page="todos" />
        <MainTodos insights={insights} categoria={categoria} />
        <Footer />
    </div>
  )

export default Categoria;