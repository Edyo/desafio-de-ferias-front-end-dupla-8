import Head from "next/head";
import Footer from "../../components/Footer";
import Header from "../../components/Header";
import MainTodos from "../../components/MainTodos";
import { GlobalStyle } from "../../styles/styles";

export const getServerSideProps = async () => {
  const res = await fetch('https://esther-api-infosights.herokuapp.com/insights/todos')
  const data = await res.json();

  return {
    props: {
      insights: data,
    },
  }
}

const Todos = ({ insights }) => (
    <div>
        <Head>
            <title>Info Insights</title>
        </Head>
        <GlobalStyle />
        <Header page="todos" />
        <MainTodos insights={insights} categoria="Todos" />
        <Footer />
    </div>
  )

export default Todos;