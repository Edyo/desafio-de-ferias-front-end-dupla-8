import styled, { createGlobalStyle, keyframes } from "styled-components"
import { merge, zoomIn, slideInDown, slideInRight, slideInLeft, fadeIn } from 'react-animations'

const openMenu = keyframes `${merge(zoomIn , slideInDown, slideInRight)}`;
const openModal = keyframes `${fadeIn}`
const slideDown = keyframes `${slideInDown}`
const slideLeft = keyframes `${slideInLeft}`

export const GlobalStyle = createGlobalStyle`
    * {
        box-sizing: border-box;
        padding: 0px;
        margin: 0px;
        text-decoration: none;
        color: white;
        font-family: Poppins;
        list-style: none;
    }
    
    html {
        font-size: 62.5%;
    }

    body {
        background-color: #1A1C17;
    }
`;

//coisas do header
export const Navbar = styled.nav`
    display: flex;
    height: 8rem;
    align-items: center;
    justify-content: space-around;
    background-color: #191919;
    position: fixed;
    width: 100%;
    top: 0;
    z-index: 10;

    #logo {
        position: relative;
        width: 12.2rem;
        height: 5rem;
    }

    @media (max-width: 768px) {
        background-color: #1A1C17;

        #logo {
            height: 6.67vw;
            width: 16.27vw;
        }
    }
`;

export const LinkNav = styled.a`
    color: ${props => (props.page ? '#7CB124' : '#8C8C8C')};
    font-size: 1.6rem;
    transition: 0.3s;
    font-weight: ${props => (props.page ? 'bold' : undefined)};
    margin-right: 3rem;
    letter-spacing: 0.2px;

    :hover {
        color: ${props => (props.page ? '#7CB124' : '#BADC41')};
    }

    @media (max-width: 768px) {
        margin-right: 0;
        margin-bottom: 3rem;
    }
`;

export const BtnHeader = styled.a`
    font-size: 1.4rem;
    transition: 0.3s;
    font-weight: 600;
    border-radius: 8px;
    background-color: #7CB124;
    padding:  1.1rem 2.4rem 1.1rem 2.4rem;

    :hover {
        background-color: #5A880E;
    }
`;

export const Hamburguer = styled.div`
    display: none;
    cursor: pointer;
    position: relative;

    @media (max-width: 768px) {
        display: flex;
        height: 4vw;
        width: 4vw;
    }
`

export const Modal = styled.div`
    display: ${props => (props.active ? 'flex' : 'none')};
    position: absolute;
    height: calc(100vh - min(18.13vw, 8rem));
    width: 100%;
    top: min(18.13vw, 8rem);
    background-color: #1A1C1799;
    z-index: 10;
    backdrop-filter: blur(4px);
    animation: 0.5s ${openModal};
`

export const LinkContainer = styled.div`
    display: flex;
    align-items: center;

    @media (max-width: 768px) {
        display: ${props => (props.active ? 'flex' : 'none')};
        position: absolute;
        flex-direction: column;
        z-index: 100;
        background-color: #252822;
        top: min(18.13vw, 8rem);
        width: 75.47%;
        border-radius: 12px;
        height: ${props => (props.active ? '30.1rem' : '0rem')};
        animation: 0.5s ${openMenu};
        justify-content: center;
    }
`
//fim das coisas do header

//inicio das coisas do corpo da pagina inicial

export const InicioSection = styled.section`
    display: flex;
    width: 100%;
    justify-content: center;
    align-items: center;
    margin-top: 14rem;
    margin-bottom: 14rem;

    @media (max-width: 768px) {
        flex-direction: column;
    }
`;

export const InicioEsq = styled.div`
    display: flex;
    flex-direction: column;
    width: max(16.41%, 31.5rem);
    margin-right: 6rem;

    #text-info {
        font-size: 1.4rem;
        align-self: center;
    }

    #text-info span {
        color: #7CB124;
        font-weight: bold;
    }

    p {
        margin-top: 4rem;
    }

    #btn-add {
        margin-top: 4.2rem;
        margin-bottom: 1.2rem;
    }

    @media (max-width: 1366px) and (min-width: 769px) {
        #btn-add {
            display: none;
        }

        #btn-todos {
            margin-top: 4.2rem;
            background-color: #7CB124;
        }

        #btn-todos:hover {
            background-color: #BADC41;
        }
    }

    @media (max-width: 768px) {
        width: 74.93%;
        margin: 0;

        #inicio-logo {
            content: url('/insightLogoMobile.svg');
        }

        p {
            margin-top: 6.3rem;
        }

        #btn-add {
            margin-top: 4.7rem;
        }

        #text-info {
            margin-top: 5.5rem;
        }
    }
`;

export const InicioDir = styled.div`
    display: flex;
    flex-direction: column;
    width: 44%;
    overflow-y: scroll;
    scrollbar-width: none;
    -ms-overflow-style: none;
    height: 69.5rem;
    padding-left: 2rem;
    padding-right: 2rem;

    ::-webkit-scrollbar {
        display: none;
    }

    @media (max-width: 1366px) {
        height: 52.9rem;
    }

    @media (max-width: 768px) {
        overflow-y: visible;
        width: 74.93%;
        margin: 0;
        padding: 0;
        height: auto;
        margin-top: 3.5rem;
    }
`;

export const Paragraph = styled.p`
    font-size: 1.6rem;

    span {
        color: #7CB124;
        white-space: nowrap;
    }
`;

export const Btn = styled.a`
    border-radius: 30px;
    background-color: ${props => (props.color)};
    width: 100%;
    padding: 0.7rem;
    text-align: center;
    transition: 0.3s;
    font-size: 16px;
    font-weight: 600;
    border-width: 0.3rem;
    border-color: #7CB124;
    border-style: solid;

    :hover {
        background-color: #BADC41;
        border-color: #BADC41;
    }

    :focus {
        outline-style: solid;
        outline-width: 0.3rem;
        outline-color: #7CB124;
    }
`;

export const Card = styled.div`
    display: flex;
    align-items: center;
    background-color: #252822;
    margin-top: 2.4rem;
    padding: 2rem 2.5rem 2.2rem 2.5rem;
    border-radius: 10px;
    transition: 0.3s;

    :hover {
        box-shadow: 0px 0px 20px #5A880E;
    }

    .card-img {
        margin-right: 2.9rem;
        height: 8rem;
        width: 8rem;
    }

    div {
        display: flex;
        flex-direction: column;
        overflow: hidden;
        width: 85%;
    }

    p {
        overflow: hidden;
        text-overflow: ellipsis;
        height: 4.8rem;
        display: -webkit-box;
        -webkit-line-clamp: 2;
        -webkit-box-orient: vertical;
    }

    .titulo-card {
        font-weight: bold;
        font-size: 1.6rem;
        white-space: nowrap;
        height: auto;
        display: block;
        margin-bottom: 0.5rem;
    }

    .categoria {
        color: #A0A4A8;
        font-size: 1.6rem;
        height: auto;
        margin-bottom: 1rem;
    }

    a {
        color: #7CB124;
        font-weight: bold;
        font-size: 1.6rem;
        margin-top: 1rem;
        transition: 0.3s;
    }

    a:hover {
        text-decoration: underline;
    }

    @media (max-width: 1366px) {
        .card-img {
            margin-right: 1rem;
            display: none;
        }

        div {
            width: 100%;
        }
    }

    @media (max-width: 768px) {
        p {
            -webkit-line-clamp: 6;
            height: auto;
        }
    }
`;
//fim das coisas do corpo da pagina inicial

//inicio do footer
export const FooterSection = styled.section`
    display: flex;
    flex-direction: column;
    width: 100%;

    #footer-cima {
        background-color: #252822;
        display: flex;
        align-items: center;
        width: 100%;
        height: 24rem;
        justify-content: space-around;
        margin-bottom: 3.5rem;
    }

    #logo-footer {
        height: 9rem;
    }

    #footer-links-container {
        display: flex;
        height: 4.5rem;
    }

    #footer-links-container a {
        margin-left: 2.5rem;
    }

    p {
        font-size: 16px;
        align-self: center;
    }

    #text-copyright {
        margin-top: 1rem;
        margin-bottom: 3.5rem;
    }

    #text-contatos span {
        font-weight: bold;
    }

    @media (max-width: 1366px) {
        #footer-cima {
            height: 20rem;
        }

        #logo-footer {
            height: 6.5rem;
            width: 15.9rem;
        }

        #footer-links-container {
            height: 3.5rem;
        }

        #footer-links-container a {
            margin-left: 2rem;
        }

        a div {
            height: 3.5rem;
            width: 3.5rem;
        }
    }

    @media (max-width: 768px) {
        #footer-cima {
            flex-direction: column;
            justify-content: center;
            height: 22.5rem;
        }

        #logo-footer {
            height: 5.6rem;
            width: 13.8rem;
            margin-bottom: 3.2rem;
        }

        #footer-links-container {
            height: 2.7rem;
        }

        #footer-links-container a {
            margin-left: 1.5rem;
        }

        a div {
            height: 2.7rem;
            width: 2.7rem;
        }

        p {
            width: 81.07%;
            text-align: center;
        }
    }
`;
//Fim do Footer

//inicio do MainTodos
export const TodosSection = styled.section`
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;

    #todos-div {
        display: flex;
        flex-direction: column;
        align-items: center;
        width: calc(63.02vw + 2.5rem);
    }

    h1 {
        font-weight: bold;
        font-size: 3.6rem;
        color: #BADC41;
        margin-top: 20rem;
        margin-bottom: 8rem;
    }

    #filtro-container {
        align-self: flex-end;
        display: flex;
        flex-direction: column;
        margin-bottom: 2.9rem;
        position: relative;
    }

    #filtro-container label {
        color: #EEEEEE;
        font-size: 1.6rem;
        margin-bottom: 1rem;
    }

    #filtro-container button {
        display: flex;
        justify-content: space-between;
        background-color: #363636;
        border: none;
        border-radius: 0.5rem;
        padding: 1rem;
        width: 29rem;
        cursor: pointer;
    }

    #filtro-container button.active {
        outline: #BADC41 solid 1px;
    }

    #filtro-container button span {
        font-size: 1.6rem;
    }

    .seta-select {
        border: solid white;
        border-width: 0 2px 2px 0;
        padding: 4px;
        transform: rotate(45deg);
        margin-right: 0.6rem;
        margin-top: 0.2rem;
    }

    #lista-todos {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        max-height: 106rem;
        margin-bottom: 5rem;
        column-gap: 2.5rem;
        width: 100%;
    }

    .card {
        width: 31.51vw;
    }

    #seletor-pagina {
        align-self: flex-end;
        align-items: center;
        justify-content: space-between;
        display: flex;
        margin-bottom: 8rem;
        width: 37.5rem;
    }

    #seletor-pagina div {
        width: 6rem;
        height: 6rem;
        border-radius: 0.5rem;
        border: 1px solid #BCBCBC;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    #seletor-pagina p {
        color: #BCBCBC;
        font-family: Inter;
        font-size: 1.7rem;
    }

    #numero-pagina {
        width: 6rem;
        height: 6rem;
        border-radius: 0.5rem;
        border: 1px solid #BCBCBC;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    #numero-pagina.page6 {
        background-color: #7CB124;
        border-color: #BADC41;
    }

    #numero-de-paginas {
        color: white;
        width: 6rem;
        height: 6rem;
        border-radius: 0.5rem;
        border: 1px solid #BADC41;
        display: flex;
        justify-content: center;
        align-items: center;
        background-color: #7CB124;
    }

    #seta-dir {
        transform: scaleX(-1);
        cursor: pointer;
    }

    #seta-esq {
        cursor: pointer;
    }

    @media (max-width: 768px) {
        h1 {
            font-size: 2.4rem;
            margin-top: 17.4rem;
            margin-bottom: 9.8rem;
        }

        #todos-div {
            width: 100%;
        }

        #filtro-container {
            margin-bottom: 7.4rem;
            align-self: center;
        }

        #filtro-container button {
            width: 28.2rem;
        }

        #lista-todos {
            flex-wrap: nowrap;
            column-gap: normal;
            align-items: center;
            height: auto;
            margin-bottom: 16.4rem;
            flex-direction: column;
            max-height: none;
        }

        .card {
            width: 75.2%;
        }

        #seletor-pagina {
            display: none;
        }
    }
`;

export const SeletorCategoria = styled.div`
    background-color: #363636;
    padding-top: 2rem;
    padding-bottom: 2rem;
    width: 29rem;
    display: ${props => (props.active ? "flex" : "none")};
    flex-direction: column;
    border: #BADC41 1px solid;
    border-radius: 1rem;
    position: absolute;
    top: 8.8rem;
    animation: 0.5s ${openModal};
    z-index: 9;

    label {
        cursor: pointer;
        width: 100%;
        padding: 1rem;
        margin: 0;
    }

    .option {
        display: none;
    }

    @media (max-width: 768px) {
        width: 28.2rem;
    }
`;
//fim do MainTodos

//inicio do MainAdicionar
export const AdicionarSection = styled.section`
    display: flex;
    flex-direction: column;
    align-items: center;

    #loading-container {
        display: none;
    }

    #loading-container.active {
        display: flex;
        flex-direction: column;
        width: 100%;
        background-color: #7CB124;
        align-items: center;
        position: fixed;
        top: 8rem;
        z-index: 10;
    }

    #loading-text-div {
        width: calc(63.02vw + 2.5rem);
        display: flex;
        margin-top: 1.9rem;
        margin-bottom: 1.5rem;
        align-items: center;
    }

    #loading-text {
        margin-left: 2.5rem;
        font-size: 1.4rem;
    }

    #loading-x {
        cursor: pointer;
    }

    #barra-loading {
        width: 100%;
        height: 0.5rem;
        background-color: #728911;
    }

    #loading-container.active #barra-progresso {
        width: 100%;
        height: 100%;
        background-color: #BADC41;
        animation: 1.5s ${slideLeft};
    }

    h1 {
        color: #BADC41;
        font-size: 3.6rem;
        font-weight: bold;
        margin-top: 16rem;
        margin-bottom: 4rem;
    }

    #form {
        display: flex;
        flex-direction: column;
        width: calc(63.02vw + 2.5rem);
        position: relative;
    }

    #div-form-cima {
        display: flex;
        column-gap: 2.5rem;
        margin-bottom: 4.9rem;
    }

    #div-form-titulo, #div-form-categoria, #div-form-link, #div-form-alt {
        display: flex;
        flex-direction: column;
        position: relative;
    }

    label {
        color: #EEEEEE;
        font-size: 1.6rem;
        margin-bottom: 1rem;
    }

    input {
        background-color: #363636;
        border-radius: 5px;
        border: none;
        color: #EEEEEE;
        padding: 1rem;
        width: 31.51vw;
        font-size: 1.6rem;
    }

    .falha {
        visibility: hidden;
        color: #DB1E1E;
        font-size: 1.4rem;
        position: absolute;
        top: 8.8rem;
    }

    .falha.active {
        visibility: visible;
    }

    #falha-descricao {
        top: 49.8rem;
    }

    #div-form-categoria {
        position: relative;
    }

    #form-categoria {
        display: flex;
        justify-content: space-between;
        background-color: #363636;
        border: none;
        border-radius: 0.5rem;
        padding: 1rem;
        width: 31.51vw;
        cursor: pointer;
    }

    #form-categoria.active {
        outline: #BADC41 solid 1px;
    }

    #form-categoria span {
        font-size: 1.6rem;
        color: #EEEEEE;
    }

    .seta-select {
        border: solid white;
        border-width: 0 2px 2px 0;
        padding: 4px;
        transform: rotate(45deg);
        height: 4px;
        margin-right: 0.6rem;
        margin-top: 0.5rem;
    }

    #seletor-filtros {
        width: 31.51vw;
    }

    #div-form-meio {
        display: flex;
        column-gap: 2.5rem;
        margin-bottom: 4.9rem;
    }

    #form-description {
        width: calc(63.02vw + 2.5rem);
        height: 20rem;
        background-color: #363636;
        border-radius: 5px;
        border: none;
        color: #EEEEEE;
        padding: 1rem;
        font-size: 1.6rem;
        resize: none;
        margin-bottom: 7.4rem;
    }

    #div-form-btn {
        width: calc(63.02vw + 2.5rem);
        display: flex;
        justify-content: space-between;
        align-items: center;
        margin-bottom: 8rem;
    }

    #form-cancel {
        color: #FF5555;
        padding: 1.5rem 4rem 1.5rem 4rem;
        font-size: 1.6rem;
        border-radius: 3rem;
        border: 1px solid #FF5555;
    }

    #form-submit {
        background-color: #7CB124;
        border-radius: 3rem;
        color: #FFFFFF;
        padding: 1.5rem 4rem 1.5rem 4rem;
        width: auto;
        cursor: pointer;
    }

    input:focus, textarea:focus {
        outline: #BADC41 1px solid;
    }

    #form-submit:hover {
        transition: 0.3s;
        background-color: #BADC41;
    }

    #form-submit:focus {
        outline: 3px #7CB124 solid;
    }

    #form-cancel:hover {
        transition: 0.3s;
        background-color: #FF5555;
        color: white;
    }

    #form-cancel:focus {
        outline: 3px #DA2B2B solid;
    }

    input.falhou {
        outline: 1px solid #DB1E1E;
    }

    input.falhou::placeholder {
        color: #DB1E1E;
    }

    #form-description.falhou {
        outline: 1px solid #DB1E1E;
    }

    #form-description.falhou::placeholder {
        color: #DB1E1E;
    }

    label.falhou {
        color: #DB1E1E;
    }

    input.passou {
        outline: 1px solid #7CB124;
        color: #7CB124;
    }

    #form-description.passou {
        outline: 1px solid #7CB124;
        color: #7CB124;
    }

    label.passou {
        color: #7CB124;
    }

    #form-categoria.passou {
        outline: 1px solid #7CB124;
    }

    #form-categoria.passou span {
        color: #7CB124;
    }

    #form-categoria.falhou {
        outline: 1px solid #DB1E1E;
    }

    #form-categoria.falhou span {
        color: #DB1E1E;
    }

    @media (max-width: 768px) {
        h1 {
            font-size: 2.4rem;
            margin-top: 15.7rem;
            margin-bottom: 8rem;
        }

        #form {
            width: 75.47%;
        }

        #div-form-cima {
            flex-direction: column;
            margin-bottom: 3.4rem;
        }

        #div-form-titulo {
            margin-bottom: 3.4rem;
        }

        input {
            width: 100%;
        }

        #form-categoria {
            width: 75.47vw;
        }

        #div-form-meio {
            flex-direction: column;
            margin-bottom: 3.4rem;
        }

        #div-form-link {
            margin-bottom: 3.4rem;
        }

        #form-description {
            width: 100%;
            margin-bottom: 4.6rem;
        }

        #div-form-btn {
            flex-direction: column;
            width: 100%;
            gap: 1.2rem;
            margin-bottom: 16.4rem;
        }

        #form-cancel {
            width: 100%;
            display: flex;
            justify-content: center;
        }

        #form-submit {
            width: 100%;
        }

        #falha-descricao {
        top: 69.2rem;
        }
    }
`;
//fim do MainAdicionar

//inicio do MainInsights
export const InsightsSection = styled.section`
    display: flex;
    flex-direction: column;
    padding-top: 8rem;
    align-items: center;

    #insights-img {
        width: 100%;
        height: 28.65vw;
        position: relative;
        margin-bottom: 12rem;
    }

    h1 {
        font-size: 3.6rem;
        color: #BADC41;
        margin-bottom: 8rem;
    }

    p {
        width: 64.32%;
        font-size: 1.6rem;
        margin-bottom: 12rem;
    }

    a {
        padding: 1rem 9.9rem 1rem 9.9rem;
        border-radius: 3rem;
        font-size: 1.6rem;
        font-weight: 600;
        background-color: #7CB124;
        margin-bottom: 16rem;
    }

    @media (max-width: 768px) {
        #insights-img {
            height: 112.53vw;
            content: url('/insightsDefaultMobile.svg');
            margin-bottom: 4.4rem;
        }

        h1 {
            font-size: 2.4rem;
            margin-bottom: 3.2rem;
        }

        p {
            width: 75.47%;
            margin-bottom: 4.4rem;
        }

        a {
            margin-bottom: 16.4rem;
        }
    }
`;